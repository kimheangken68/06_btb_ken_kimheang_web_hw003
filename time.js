"use strict";

const watch = document.getElementById('watch');
const time = setInterval(function () { myTime(); }, 1000);

function myTime() {
    const d = new Date();
    var showTime = d.toLocaleString('en-GB', { day: 'numeric', month: 'short', year: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true });
    watch.innerHTML = showTime;
}

var btn = document.getElementById('btn');
var startTime;
var endTime;
var total = 0;
var riel = 500;
var divH;
var modMin;
var rielMin;
function start() {

    if (btn.value == "start") {
        startTime = new Date();

        btn.value = "stop";
        let myIcons = "<ion-icon name='stop-outline'></ion-icon>";
        document.getElementById('btn').innerHTML = `${myIcons}Stop`;
        document.getElementById('btn').setAttribute('class', 'bg-red-500 h-10  w-1/4 mt-5 mb-5 font-bai1  text-white py-2 px-4 rounded');
        document.getElementById('starts').innerHTML = startTime.toLocaleString('en-GB', {  hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true });

    } else if (btn.value == "stop") {
        endTime = new Date();
        document.getElementById('stops').innerHTML = endTime.toLocaleString('en-GB', {  hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true });
        let myIcons = "<ion-icon name='trash-bin-outline'></ion-icon>";
        document.getElementById('btn').innerHTML = `${myIcons} Clear`;
        document.getElementById('btn').setAttribute('class', 'bg-yellow-500 h-10 font-bai1  w-1/4 mt-5 mb-5  text-white py-2 px-4 rounded')

        total = parseInt((endTime - startTime) / 60000);

        if (total >= 0 && total <= 15) {
            riel = 500;
        } else if (total >= 16 && total <= 30) {
            riel = 1000;
        } else if (total >= 31 && total <= 60) {
            riel = 1500;
        } else if (total > 60) {
            divH = parseInt(total / 60);
            modMin = total % 60;

            if (modMin >= 0 && modMin <= 15) {
                rielMin = 500;
            } else if (modMin >= 16 && modMin <= 30) {
                rielMin = 1000;
            } else if (modMin >= 31, modMin <= 60) {
                rielMin = 1500;
            }

            riel = (divH * 1500) + (rielMin);

        }
        document.getElementById('show-min').innerHTML = total;
        document.getElementById('show-riel').innerHTML = riel;
        btn.value = "clear";

    } else if (btn.value == "clear") {
        btn.value = "start";
        let myIcons = "<ion-icon name='play-outline'></ion-icon>";
        document.getElementById('btn').innerHTML = `${myIcons} Start`;
        document.getElementById('btn').setAttribute('class', 'bg-pink-300 h-10 font-bai1  w-1/4 mt-5 mb-5  text-white py-2 px-4 rounded')
        document.getElementById('starts').innerHTML = '00';
        document.getElementById('stops').innerHTML = '00';
        document.getElementById('show-min').innerHTML = 0;
        document.getElementById('show-riel').innerHTML = 0;
    }

}




